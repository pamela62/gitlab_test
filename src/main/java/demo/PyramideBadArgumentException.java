package demo;

public class PyramideBadArgumentException extends RuntimeException {
    public PyramideBadArgumentException(){

    }

    public PyramideBadArgumentException(String message){
        super(message);
    }
}
