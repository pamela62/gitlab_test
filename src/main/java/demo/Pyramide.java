package demo;

import java.util.ArrayList;
import java.util.List;

public class Pyramide {


    public List<String> Pyramide (Integer taille){
        if(taille<1) {
            throw new PyramideBadArgumentException("La taille doit être supérieur à zero" +  taille);
        }


        List<String> pyr = new ArrayList<>();
        for(int i=0;i<taille;i++){
            pyr.add("*".repeat(i+1));
        }
        for(int i=taille-1;i>0;i--){
            pyr.add("*".repeat(i+1));
        }
        return  pyr;
    }
}
