package demo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PyramideTest {

    static Pyramide pyramide = new Pyramide();

    @DisplayName("La liste doit être de type string non vide")
    @ParameterizedTest
    @ValueSource(ints = {1,2,7,23})
    //@CsvSource({"1,2", "4,5"})
    @Test
    void pyramide(int Taille) {
        List<String> result = pyramide.Pyramide(Taille);
        assertNotNull(result);
        for (String s : result){
            assertNotNull(s);
        }

    }

    @DisplayName("Elle doit retrouner une liste de longueur (taille)*2-1")
    @ParameterizedTest
    @ValueSource(ints={1,2,3,65})
    public void testBonneLongueur(int taille){
        List<String> result = pyramide.Pyramide(taille);
        assertEquals(taille*2-1, result.size());
    }


    /*@DisplayName("Elle doit retourner une exception si la taille est inférieur ou égale a 0")
    @ParameterizedTest
    @ValueSource(ints={1,2,3,65})
    public void testTaille(int taille){
        List<String> result = pyramide.Pyramide(taille);
        if(result.size() <=0)
        {
            throw new IllegalArgumentException("la taille doit être supérieur ou égale à zero");
        }
    }*/

    @DisplayName("Les String sont composé d'*")
    @ParameterizedTest
    @ValueSource(ints={1,2,3,65})
    public void testCompositionPyramide(int taille){
        List<String> result = pyramide.Pyramide(taille);

        if( result.contains("*") == false)
        {
            throw new IllegalArgumentException("Les Strings doivent être composé d'étoiles");
        }

    }

    /*@DisplayName("Elle doit retourner une exception si la taille est inférieur ou égale a 0")
    @ParameterizedTest
    @ValueSource(ints={1,2,3,65})
    public void testTailleException(int taille){
        assertThrows(PyramideBadArgumentException.class,()-> pyramide.Pyramide(taille));
    }*/

    @DisplayName("Les String sont composé d'*")
    @ParameterizedTest
    @ValueSource(ints={1,2,3,65})
    public void testCompositionPyramideException(int taille){
        List<String> result = pyramide.Pyramide(taille);
        for(String s : result){
            assertTrue(s.matches("^[*]+$"));
        }

    }

   /* @ParameterizedTest
    @ValueSource(ints= {2,5,8})
    @DisplayName("Les string ont une longueur ou égale à la taille")
    public void testLongueurString(int taille)
    {
        List<String> list = pyramide.Pyramide(taille);
        assertEquals(taille, list.stream().ma);
    }*/


}